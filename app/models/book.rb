class Book < ApplicationRecord

  has_many :issued_books
  has_many :users, through: :issued_books
  validates :isbn, presence: true, length: { is: 13},
            uniqueness: {case_sensitive: false}
  validates :name, presence: true,length: { maximum: 100 }
  validates :author, presence: true,length: { maximum: 50 }
  validates :total_copies, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 1}
  validates :available_copies, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0}

end
