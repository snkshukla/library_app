class BooksController < ApplicationController

  def index
    @books = Book.all
    if @books.empty?      #if there are no books
      flash[:danger] = "There are no books in the lirary!! You can add a book here!"
      redirect_to addbook_path
    end
  end

  def new
    @book = Book.new
  end

  def show
    @book = Book.find_by(id: params[:id])
    if @book.nil?         #if the book is not present in the database
      flash[:danger] = "There is no such book in the lirary"
      redirect_to books_show_path
    end
  end

  def create
    @book = Book.new(book_params)
    @book.update_attribute(:available_copies, params[:book][:total_copies])  #initially the available copies are equal to the number of total copies
    if @book.save
      flash[:success] = "The book has been successfully added to lirary"
      redirect_to @book
    else    #The error will be displayed. We can be more specific like based on error we can ask if more copies need to be added.
      render 'new'
    end
  end

  #filtering data
  private
  def book_params
    params.require(:book).permit(:isbn, :name, :author, :total_copies)
  end
end
