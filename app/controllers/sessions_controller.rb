#the sessions class for creating and destroying session
class SessionsController < ApplicationController
  skip_before_action :require_login_as_admin, only: [:new,:create]
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.user_type == "A"
        log_in user
        flash[:success] = "Welcome #{user.name}"
        redirect_to user
      else
        flash.now[:danger] = 'Sorry...but only the admins can login currently!!'
        render 'new'
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end

  end

  def destroy
    log_out
    redirect_to root_url
  end
end
