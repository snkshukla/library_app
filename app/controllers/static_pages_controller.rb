#This controller contains static pages used across the app.
class StaticPagesController < ApplicationController
  skip_before_action :require_login_as_admin
  def home
    if logged_in?
      redirect_to current_user
    end
  end

  def help
  end

  def about
  end

  def contact
  end
end
