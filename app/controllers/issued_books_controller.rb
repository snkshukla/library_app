class IssuedBooksController < ApplicationController

  def new
    @issued_book = IssuedBook.new
  end

  def show

  end

  def index
    @issued_books = IssuedBook.all
    if @issued_books.empty?
      flash[:danger] = "No books issued yet!! You can issue a book here"
      redirect_to issue_book_path
    end
  end

  def create

    #find the user and the book by the IDs provided
    book = Book.all.find_by(id: params[:issued_book][:book_id])
    user = User.all.find_by(id: params[:issued_book][:user_id])

    #checking if the user and the book exit
    if book.nil?
      flash[:danger] = 'Book not found!!'
      redirect_to issue_book_path
    elsif user.nil?
      flash[:danger] = 'User not found!!'
      redirect_to issue_book_path
    elsif book.available_copies < 1       #check the availability before proceeding. This code can be shifted to model with the help custom validations
      flash[:danger] = 'No copy of this book is available right now!!'
      redirect_to issue_book_path
    else          #save the book finally
      @issued_book = IssuedBook.new(book_id: params[:issued_book][:book_id],
                                    user_id: params[:issued_book][:user_id],
                                    issue_date: Date.today, exp_return_date: Date.today+1.month)
      begin
        #A transaction is used while saving the details because two models need to be updated simultaneously:
        #1. the book's availability has to be decreased by one and
        #2. the issue detail is to be saved otherwise rollback
        ActiveRecord::Base.transaction do
          @issued_book.save!
          book.update_attribute( :available_copies, book.available_copies-1 )
          book.save!
        end
        flash[:success] = "The book has been successfully issued to #{user.name}"
        redirect_to @issued_book
      rescue => e     #handle the error if transaction failed.
        flash[:danger] = "The book could not be issued.. please try again!!"
        render 'new'
      end
    end
  end
end
