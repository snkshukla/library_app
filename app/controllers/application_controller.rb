class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  include ApplicationHelper

  #it makes sure that that the user is logged in as admin before accessing library
  before_action :require_login_as_admin
end
