module ApplicationHelper

  #This function will facilitate the title
	def full_title(page_title = '')
    base_title = "Library"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  #the method below is put here because it will then be available across all the controllers to ensure that user is logged in before proceeding
  def require_login_as_admin
    if current_user == nil
      flash[:danger] = 'You have not logged in yet, login to see this page!!'
      redirect_to login_path
      return false
    elsif current_user.user_type != "A"
      flash[:danger] = 'You are not authorized to view this page!!'
      redirect_to root_path
      return false
    else
      return true
    end
  end
end
