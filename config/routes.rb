Rails.application.routes.draw do

  get '/issue_book', to: 'issued_books#new'
  post '/issue_book', to: 'issued_books#create'
  get '/issued_books', to: 'issued_books#index'

  get '/books/new', to: 'books#new'

  get '/books/show', to: 'books#index'

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  root 'static_pages#home'
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'

  get '/addbook', to: 'books#new'
  post '/addbook', to: 'books#create'

  resources :users
  resources :books
  resources :issued_books

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
