# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#create an admin user
User.create(email: "admin@admin.com", name: "admin", user_type: "A", password: "admins", password_confirmation: "admins")

#create normal users
User.create(email: "user1@gmail.com", name: "user #1", user_type: "N", password: "nusers", password_confirmation: "nusers")
User.create(email: "user2@gmail.com", name: "user #2", user_type: "N", password: "nusers", password_confirmation: "nusers")
User.create(email: "user3@gmail.com", name: "user #3", user_type: "N", password: "nusers", password_confirmation: "nusers")
User.create(email: "user4@gmail.com", name: "user #4", user_type: "N", password: "nusers", password_confirmation: "nusers")
User.create(email: "user5@gmail.com", name: "user #5", user_type: "N", password: "nusers", password_confirmation: "nusers")

#create new books
Book.create(isbn: "1234567890121", name: "The Lost Symbol", author: "Dan Brown", available_copies: 20, total_copies: 20)
Book.create(isbn: "1234567890122", name: "Jungle Book", author: "Rudyard Kipling", available_copies: 10, total_copies: 10)
Book.create(isbn: "1234567890123", name: "The Krishna Key", author: "Ashwin Sanghi", available_copies: 4, total_copies:4)
Book.create(isbn: "1234567890124", name: "To Kill a Mockingbird", author: "Harper Lee", available_copies: 2, total_copies:2)
Book.create(isbn: "1234567890125", name: "Inferno", author: "Dan Brown", available_copies: 20, total_copies: 20)


