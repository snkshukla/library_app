require 'test_helper'

class IssuedBookTest < ActiveSupport::TestCase
  def setup
    @issued_book = IssuedBook.new(book_id: 2,user_id: 3,issue_date: Date.today,exp_return_date: Date.today+1.month)
  end

  test "should be valid" do
    assert @issued_book.valid?
  end

  test "book id should be present and valid" do
    @issued_book.book_id = "  "
    assert_not @issued_book.valid?
    @issued_book.book_id = 14
    assert_not @issued_book.valid?
  end

  test "user id should be present and valid" do
    @issued_book.user_id = "  "
    assert_not @issued_book.valid?
    @issued_book.user_id = 14
    assert_not @issued_book.valid?
  end


end
