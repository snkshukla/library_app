require 'test_helper'

class BookTest < ActiveSupport::TestCase
  def setup
    @book = Book.new(isbn: "1232345434324", name: "book tile", author: "author", available_copies: 20, total_copies: 20 )
  end

  test "should be valid" do
    assert @book.valid?
  end

  test "isbn should be present" do
    @book.isbn = "   "
    assert_not @book.valid?
  end

  test "isbn should be exact 13 characters long" do
    @book.isbn = "a" * 14
    assert_not @book.valid?, "valid on more than 13 characters"
    @book.isbn = "a" * 12
    assert_not @book.valid?, "valid on less than 13 characters"
  end

  test "isbn should be unique" do
    dup_book = @book.dup
    dup_book.isbn = @book.isbn.upcase
    @book.save
    assert_not dup_book.valid?
  end

  test "name should be present" do
    @book.name = "  "
    assert_not @book.valid?
  end

  test "name should not be too long" do
    @book.name = "a" * 101
    assert_not @book.valid?
  end

  test "author should be present" do
    @book.author = "   "
    assert_not @book.valid?
  end

  test "author should not be too long" do
    @book.author = "a" * 51
    assert_not @book.valid?
  end

  test "total copies should be more than or equal to 1" do
    @book.total_copies = 0
    assert_not @book.valid?
  end


end
