require 'test_helper'

class IssuedBooksControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:shubham)
  end



  test "should not get new without login" do
    get issue_book_path
    assert_response :found
    assert_redirected_to login_path
  end

  test "should not get show without login" do
    get issued_books_path
    assert_response :found
    assert_redirected_to login_path
  end

  test "should get new and show when logged in" do
    login_user_as_admin
    get issued_books_path
    assert_response :success
    get issue_book_path
    assert_response :success
  end

  test "should create issued book" do
    login_user_as_admin
    available_copies = Book.find_by(id: 2).available_copies
    post(issue_book_path, issued_book: { book_id: 2, user_id: 3 })
    assert_response :found
    assert_not_nil IssuedBook.find_by(book_id:2, user_id: 3)
    assert_equal(available_copies-1,Book.find_by(id: 2).available_copies)
  end



end
