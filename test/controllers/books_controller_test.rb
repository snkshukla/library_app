require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:shubham)
  end

  test "should get new" do
    login_user_as_admin
    get addbook_path
    assert_response :success
  end

  test "should get show" do
    login_user_as_admin
    get books_show_path
    assert_response :success
  end

  test "should create book" do
    login_user_as_admin
    post(addbook_path, book: { isbn: "1838473828473",name: "Love letter", author: "ravi", total_copies: 20 })
    assert_response :found
    assert_not_nil Book.find_by(isbn: "1838473828473")
  end


end
