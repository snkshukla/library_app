require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:shubham)
  end

  test "should redirect show when not logged in" do
    get "/users/#{@user.id}"
    assert_response :found
    assert_redirected_to login_path
  end

  test "should show user when logged in" do
    login_user_as_admin
    assert_response :found
    assert_redirected_to "/users/#{@user.id}"
    get "/users/#{@user.id}"
    assert_response :success
  end



end
