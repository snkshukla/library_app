require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:shubham)
  end

  test "should get new" do
    get login_path
    assert_response :success
  end

  test "should create session" do
    post(login_path, session: { email: @user.email,password: @user.password })
    assert_response :success
    assert_not_nil is_logged_in?
  end

  test "should destroy session" do
    login_user_as_admin
    delete(logout_path, session: :user_id)
    assert_response :found
    assert_redirected_to root_path
  end
end
