ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  def is_logged_in?
    !session[:user_id].nil?
  end

  def login_user_as_admin
    get login_path
    post(login_path, session: { email: 'snkshukla@gmail.com', password: 'password' })
  end


  # Add more helper methods to be used by all tests here...
end
