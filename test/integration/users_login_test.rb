require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:shubham)
  end

  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { email: "", password: "" } }
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "login with valid information followed by logout" do
    get login_path
    post login_path, params: { session: { email: @user.email,
                                          password: 'password' } }
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", addbook_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "show user without login" do
    get "/users/#{@user.id}"
    assert_not is_logged_in?
    assert_redirected_to login_path
    follow_redirect!
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "access show books without login" do
    get books_show_path
    assert_not is_logged_in?
    assert_redirected_to login_path
    follow_redirect!
    assert_not flash.empty?, "No flash found after redirect"
    get root_path
    assert flash.empty?
  end

  test "add books without login" do
    get addbook_path
    assert_not is_logged_in?
    assert_redirected_to login_path, "not redirected if not logged in"
    follow_redirect!
    assert_not flash.empty?, "No flash found after redirect"
    get root_path
    assert flash.empty?
  end
end
