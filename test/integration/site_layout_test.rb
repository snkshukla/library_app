require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout_links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", login_path,  count: 2
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path

    login_user_as_admin
    get "/users/#{session[:user_id]}"
    assert_template "users/show"
    assert_select "a[href=?]", "/books/show"
    assert_select "a[href=?]", "/issue_book"
    assert_select "a[href=?]", "/addbook"
  end

end
