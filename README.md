# Web application for a library.
> An application on Ruby on rails.

This is a web application which basically does the following:

1. Shows all the books in the library along with their availability.

2. Access is currently restricted to only admin users.

3. The books can be tracked for availability.

4. A book can be issued to users if it is available.

5. The admin users can login and logout. Session is stored which will be destroyed when user closes the browser (This app does not use cookies).

It also ensures that a user is logged in before they are able to see any of the details.

## Installation and setup:

After cloning the repo, run 
```shell
$ rails db:setup 
```
Or, if this fails you can run following commands in order:
```shell
$ rails db:schema:load
$ rails db:seed
```
This will populate the database with some data:

* An admin is added with the following credentials:

``` 
username/email: admin@admin.com
password: admins
```
* Some books are also added to the library database.
* This user can also be used on heroku implementation.
* Also, currently the UI for adding the users is not present, so these users can be used to test the application.

## Production App:

Deployed on Heroku: [https://snkshukla-lib.herokuapp.com](https://snkshukla-lib.herokuapp.com)


## Meta 

Shubham Shukla